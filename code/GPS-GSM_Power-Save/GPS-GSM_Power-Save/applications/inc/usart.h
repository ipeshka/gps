
#include "stm32f10x.h"

#define TX_BUF_SIZE   128
#define RX_BUF_SIZE   1300

#define USART1_TIMEOUT		1		// ����� �������� � ��, �� ��������� �������� ����� ��������� �����������
#define USART2_TIMEOUT		1  	// ����� �������� � ��, �� ��������� �������� ����� ��������� �����������

#define USART1_TXD      GPIO_Pin_9
#define USART1_RXD      GPIO_Pin_10

#define	USART2_TXD			GPIO_Pin_2
#define	USART2_RXD			GPIO_Pin_3
#define	USART2_CTS			GPIO_Pin_0
#define	USART2_RTS			GPIO_Pin_1

#define RX_BUSY						2
#define RX_COMPLETE       1
#define TX_COMPLETE       1
#define RX_NO_COMPLETE    0
#define TX_NO_COMPLETE    0
#define RX_ERROR        	-1
#define TX_ERROR       		-1

//����������� ������� ASCII
#define CTRL_Z						0x1A

unsigned char Usartx, tx_restart;

struct TBufferUsart {
  //unsigned int rxcnt;
  uint16_t txcnt;				// ������� ������� ����������� �����
  uint16_t rxlength;		// ���-�� �������� ����
  uint16_t txlength;		// ���-�� ������������ ����
  int8_t rxflag;			// ������ ������: �������� / �� ��������
  int8_t txflag;			// ������ ��������: ��������� / �� ���������
  char rxbuf [RX_BUF_SIZE];	// ����� ���������
  char txbuf [TX_BUF_SIZE];	// ����� �����������
};

volatile struct TBufferUsart BufferUsart1;
volatile struct TBufferUsart BufferUsart2;

void USART1_Init(void);
void USART2_Init(void);
//int SendChar (char ch);
void Puts(USART_TypeDef* USARTx, const void *str);
void SendBuf(USART_TypeDef* USARTx);
void Put_Char(USART_TypeDef* USARTx, uint8_t data);
void PutsDec8(USART_TypeDef* USARTx, uint8_t decnum);
void PutsDec16(USART_TypeDef* USARTx, uint16_t decnum);
void PutsDec32(USART_TypeDef* USARTx, uint32_t decnum);
void BufferInit (void);
const void *Gets (USART_TypeDef* USARTx);
void USART_Timeout(USART_TypeDef* USARTx);
void IT_RX_Disable (USART_TypeDef* USARTx);
void IT_RX_Enable (USART_TypeDef* USARTx);
void USARTx_Disable (USART_TypeDef* USARTx);
void USARTx_Enable (USART_TypeDef* USARTx);

