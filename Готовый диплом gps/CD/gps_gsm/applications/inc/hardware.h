
#define		OK			(0)
#define		ERROR		!(OK)

#define		GPIO_LED0		GPIOB
#define		LED0				GPIO_Pin_15

typedef enum {OFF = 0, ON} TPower;

void HardwareInit (void);
void LED0_Power (TPower power);



