


#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_usart.h"
#include "misc.h"
#include <stdio.h>
#include "usart.h"
#include "timer.h"
#include "delay.h"

USART_InitTypeDef USART_InitStructure;


/*
 * ������������� USART1
 */
void USART1_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  // ��������� ������������ ����� A
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  //��������� ������������ USART2
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

  // ������������� ����� �� �����
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_InitStructure.GPIO_Pin = USART1_RXD;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // ������������� ����� �� ��������
  //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
  GPIO_InitStructure.GPIO_Pin = USART1_TXD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);


  // ������������� �����
  USART_StructInit(&USART_InitStructure);
  // �������� UART MAX-6 �� ��������� 9600
  USART_InitStructure.USART_BaudRate = 9600;
  USART_Init(USART1, &USART_InitStructure);
  USART_ITConfig(USART1, USART_IT_RXNE /*| USART_IT_CTS*/, ENABLE);
  USART_Cmd(USART1, ENABLE);

  // ������������� ������
  InitBasicTimer (TIM6);
  PreloadBasicTimer (TIM6, USART1_TIMEOUT);

}


/*
 * ������������� USART2
 */
void USART2_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // ��������� ������������ ����� A
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  // ��������� ������������ USART2
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

  // ������������� ����� �� �����
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_InitStructure.GPIO_Pin = USART2_RXD | USART2_CTS;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // ������������� ����� �� ��������
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Pin = USART2_TXD | USART2_RTS;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  // ������������� �����
  USART_StructInit(&USART_InitStructure);
  USART_Init(USART2, &USART_InitStructure);
  USART_ITConfig(USART2, USART_IT_RXNE /*| USART_IT_CTS*/, ENABLE);
  USART_Cmd(USART2, ENABLE);

  // ������������� ������
  InitBasicTimer (TIM7);
  PreloadBasicTimer (TIM7, USART2_TIMEOUT);

}


/*
 * ������� �������� ������ ����� ���������� �� ����������� ������
 */
void Puts(USART_TypeDef* USARTx, const void *str)
{
	uint8_t *s = (uint8_t *) str;
	volatile struct TBufferUsart *buffer;

	if (USARTx == USART1)	buffer = &BufferUsart1;
	if (USARTx == USART2)	buffer = &BufferUsart2;

	while (*s)
	{
		buffer->txbuf[buffer->txlength++] = *s++;
	}
	// ��������� ���������� �� ����������� ������ tx
	USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);

	while (buffer->txflag != TX_COMPLETE);
	buffer->txflag = TX_NO_COMPLETE;

	//PutChar();

}


/*
 * ������� �������� ������
 */
void SendBuf(USART_TypeDef* USARTx){
	volatile struct TBufferUsart *buffer;

	if (USARTx == USART1)	buffer = &BufferUsart1;
	if (USARTx == USART2)	buffer = &BufferUsart2;
	// ��������� ���������� �� ����������� ������ tx
	USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);

	while (buffer->txflag != TX_COMPLETE);
	buffer->txflag = TX_NO_COMPLETE;
}


/*
 * ������� �������� 1 �����
 */
void Put_Char(USART_TypeDef* USARTx, uint8_t data)
{

	volatile struct TBufferUsart *buffer;

	if (USARTx == USART1)	buffer = &BufferUsart1;
	if (USARTx == USART2)	buffer = &BufferUsart2;

	buffer->txbuf[buffer->txlength] = data;
	buffer->txbuf[++(buffer->txlength)] = '\0';

	// ��������� ���������� �� ����������� ������ tx
	USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);

	while (buffer->txflag != TX_COMPLETE);
	buffer->txflag = TX_NO_COMPLETE;
}


/*
 * ������� �������� 8-������� ����� � ���������� �������
 */
void PutsDec8(USART_TypeDef* USARTx, uint8_t decnum)
{
	const uint8_t size = 3;
	uint8_t num[size];
	uint32_t multiplier;
	uint8_t i;
	uint8_t flag = 0;
	volatile struct TBufferUsart *buffer;

	if (USARTx == USART1)	buffer = &BufferUsart1;
	if (USARTx == USART2)	buffer = &BufferUsart2;

	// ��������������� ����� � ������
	for (i = 0, multiplier = 1; i < size; i++, multiplier *= 10){
		num[i] = (decnum / multiplier) % 10;
	}
	// ��������� � ������� �������� ������� 0
	i = (sizeof(num) / sizeof(num[0]));
	while(i){
		--i;
		// ���� ������� ��������� ������� 0, �� �������� ��������
		// ��� ���� ���� ��������� ������ 0, �� ������� 0
		if ((i) && (!num[i]) && (!flag))	{
			buffer->txbuf[buffer->txlength] = ' ';
		}
		else {
			// ���� ������� ����� > 0, ���������� ����
			// ��� ����� �������� ������ 0 ��������
			flag = !flag;
			buffer->txbuf[buffer->txlength] = '0' + num[i];
		}
		buffer->txlength++;
	}
	// ��������� ���������� �� ����������� ������ tx
	USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);
	while (buffer->txflag != TX_COMPLETE);
	buffer->txflag = TX_NO_COMPLETE;
}

/*
 * ������� �������� 16-������� ����� � ���������� �������
 */
void PutsDec16(USART_TypeDef* USARTx, uint16_t decnum)
{
	const uint8_t size = 5;
	uint8_t num[size];
	uint32_t multiplier;
	uint8_t i;
	uint8_t flag = 0;
	volatile struct TBufferUsart *buffer;

	if (USARTx == USART1)	buffer = &BufferUsart1;
	if (USARTx == USART2)	buffer = &BufferUsart2;
	// ��������������� ����� � ������
	for (i = 0, multiplier = 1; i < size; i++, multiplier *= 10){
		num[i] = (decnum / multiplier) % 10;
	}

	// ��������� � ������� �������� ������� 0
	i = (sizeof(num) / sizeof(num[0]));
	while(i){
		--i;
		// ���� ������� ��������� ������� 0, �� �������� ��������
		// ��� ���� ���� ��������� ������ 0, �� ������� 0
		if ((i) && (!num[i]) && (!flag))	{
			buffer->txbuf[buffer->txlength] = ' ';
		}
		else {
			flag = !flag;
			buffer->txbuf[buffer->txlength] = '0' + num[i];
		}
		buffer->txlength++;
	}
	// ��������� ���������� �� ����������� ������ tx
	USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);
	while (buffer->txflag != TX_COMPLETE);
	buffer->txflag = TX_NO_COMPLETE;
}

/*
 * ������� �������� 32-������� ����� � ���������� �������
 */
void PutsDec32(USART_TypeDef* USARTx, uint32_t decnum)
{
	const uint8_t size = 10;
	uint8_t num[size];
	uint32_t multiplier;
	uint8_t i;
	uint8_t flag = 0;
	volatile struct TBufferUsart *buffer;

	if (USARTx == USART1)	buffer = &BufferUsart1;
	if (USARTx == USART2)	buffer = &BufferUsart2;
	// ��������������� ����� � ������
	for (i = 0, multiplier = 1; i < size; i++, multiplier *= 10){
		num[i] = (decnum / multiplier) % 10;
	}

	// ��������� � ������� �������� ������� 0
	i = (sizeof(num) / sizeof(num[0]));
	while(i){
		--i;
		// ���� ������� ��������� ������� 0, �� �������� ��������
		// ��� ���� ���� ��������� ������ 0, �� ������� 0
		if ((i) && (!num[i]) && (!flag))	{
			buffer->txbuf[buffer->txlength] = ' ';
		}
		else {
			flag = !flag;
			buffer->txbuf[buffer->txlength] = '0' + num[i];
		}
		buffer->txlength++;
	}
	// ��������� ���������� �� ����������� ������ tx
	USART_ITConfig(USARTx, USART_IT_TXE, ENABLE);
	while (buffer->txflag != TX_COMPLETE);
	buffer->txflag = TX_NO_COMPLETE;
}


/*
 * ������� ������ ������ ����� ����������
 */
const void * Gets (USART_TypeDef* USARTx){
	volatile struct TBufferUsart *buffer;

	if (USARTx == USART1)	buffer = &BufferUsart1;
	if (USARTx == USART2)	buffer = &BufferUsart2;

	while (buffer->rxflag != RX_COMPLETE);
	buffer->rxflag = RX_NO_COMPLETE;
	return (const void *)buffer->rxbuf;
}


/*
 * ������������� �������
 */
void BufferInit (void)
{
  BufferUsart1.txcnt  = 0;
  BufferUsart1.txlength = 0;
  BufferUsart1.rxlength = 0;
  BufferUsart1.rxflag = RX_NO_COMPLETE;
  BufferUsart1.txflag = TX_NO_COMPLETE;
  BufferUsart2.txcnt  = 0;
  BufferUsart2.txlength = 0;
  BufferUsart2.rxlength = 0;
  BufferUsart2.rxflag = RX_NO_COMPLETE;
  BufferUsart2.txflag = TX_NO_COMPLETE;
}


/*
 * ������� ��������� ����� ������, ���� �� ���� �������� �� ������ � ������� ��������
 */
void USART_Timeout(USART_TypeDef* USARTx){

	if (USARTx == USART1){
		// ��������� �������
		//PreloadBasicTimer (TIM6, USART1_TIMEOUT);
		StartBasicTimer(TIM6);
	}

	if (USARTx == USART2){
		// ��������� �������
		//PreloadBasicTimer (TIM7, USART2_TIMEOUT);
		StartBasicTimer(TIM7);
	}
}

/*
 * ������� ���������� ���������� �� ������
 */
void IT_RX_Disable (USART_TypeDef* USARTx){
	USART_ITConfig(USARTx, USART_IT_RXNE, DISABLE);
}


/*
 * ������� ��������� ���������� �� ������
 */
void IT_RX_Enable (USART_TypeDef* USARTx){
	USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
}


/*
 * ��������� USART
 */
void USARTx_Disable (USART_TypeDef* USARTx){
	USART_Cmd(USARTx, DISABLE);
}

/*
 * �������� USART
 */
void USARTx_Enable (USART_TypeDef* USARTx){
	USART_Cmd(USARTx, ENABLE);
	DelayMs(50);
}

