#include "stm32f10x.h"
#include "hardware.h"
#include "delay.h"
#include "usart.h"
#include "sim900d.h"
#include "max-6.h"


/*
#define		OK			(0)
#define		ERROR		!(OK)
*/

#define   GPS_SLEEP		0
#define   GPS_COLD		1
#define   GPS_HOT			2

uint8_t		StatusGPS = 0;


/*
 * ����������� � ������ ������������� ������ � ���� ������ ������
 */
static void _ErrorTask (void){
	while(1){
		LED0_Power(ON);
		DelayMs(200);
		LED0_Power(OFF);
		DelayMs(200);
	}
}


/*
 * ��������� � ������������� GSM ������, ����� ��������� ����������
 */
static int _StartTask (void){
	uint8_t i;

	// �������� SIM900
	if (Sim900d_Power(ON))	return ERROR;

	if (Sim900d_Init()){
		return ERROR;
	}
	Max6_Init();
	Max6_ControlGPS(COLDSTART);
	// ���������� ������������� ������� ������������� �������
	for (i = 3; i; i--){
		LED0_Power(ON);
		DelayMs(500);
		LED0_Power(OFF);
		DelayMs(500);
	}
	Sim900d_ControlSleep(ON);
	return OK;
}

/*
 * ������ �������
 */
static int _ReadTask (void/*int8_t *pPhone_Number*/){
	uint8_t index = 0;
	int8_t number[13];
	int8_t *pnumber = number;
	int8_t time[10];
	int8_t *ptime = time;
	int8_t date[10];
	int8_t *pdate = date;
	int8_t msg[100];
	int8_t *pmsg = msg;
	char *psystem_number = PC_NUMBER;
	char *psystem_cmd = CMD_READ;

	// ������� ���� �������� ���������
	while (index == 0){
		index = Sim900d_EventNewSMS();
	}
	Sim900d_ControlSleep(OFF);
	// ������ SMS � ��������� ��������
	Sim900d_ReadSMS(index, pnumber, pdate, ptime, pmsg);
//	Sim900d_ReadSMS(index, pPhone_Number, pdate, ptime, pmsg);
	// ������ SMS � ��������� ��������
	if (Sim900d_DelIndexSMS(index))	return ERROR;
	// �������� ������ �� ������������ PC_NUMBER
	while (*psystem_number){
		if (*pnumber++ != *psystem_number++)	{
			return ERROR;
		}
	}
	while (*psystem_cmd){
		if (*pmsg++ != *psystem_cmd++)	{
			return ERROR;
		}
	}

	return OK;
}

/*
 * ������ � �������� ���������
 */
static int _SendCoordinateTask (void *number/*int8_t *pPhone_Number*/){
	uint8_t i;
	int8_t latitude[11];
	int8_t longitude[12];
	int8_t time[12];
	int8_t msg[36];
	int8_t *pmsg = msg;

	LED0_Power(ON);
	if (Max6_ReadCoordinate(&latitude[0], &longitude[0], &time[0]))	return ERROR;
	DelayMs(200);
	LED0_Power(OFF);

	for (i = 0; i < sizeof(latitude) / sizeof(latitude[0]); i++){
		*pmsg++ = latitude[i];
	}
	*pmsg++ = '\r';
	*pmsg++ = '\n';

	for (i = 0; i < sizeof(longitude) / sizeof(longitude[0]); i++){
		*pmsg++ = longitude[i];
	}
	*pmsg++ = '\r';
	*pmsg++ = '\n';

	for (i = 0; i < sizeof(time) / sizeof(time[0]); i++){
		*pmsg++ = time[i];
	}
	*pmsg = '\0';

	if (Sim900d_SendSMS(number, msg))	return ERROR;

	return OK;
}

/*
 * ������� ���������� GSM ������
 */
static int _StopTask (void){
	// ��������� SIM900
	if (!Sim900d_Power(OFF)){
	}
	else {
		// ���� ���������� �� ���� ������������
		return ERROR;
	}
	return OK;
}

/*
 * �������� ����
 */
static int _MainTask (void){

/*
	int8_t latitude[11];
	int8_t longitude[12];
	int8_t time[12];
*/
	// ��������� Sleep GSM-������

	// ���� ���������� ������������
	/*if (!Max6_ReadCoordinate(&latitude[0], &longitude[0], &time[0])){
		// �� ��������� GPS ������ � ������ �����

		StatusGPS = SLEEP;
	}*/
//	int8_t system_phone_number[13];
//	int8_t *psystem_phone_number = system_phone_number;

	if (!_ReadTask(/*psystem_phone_number*/))	{
		/*if (StatusGPS == SLEEP){
			// ������� �����
		}*/
		if (!_SendCoordinateTask(PC_NUMBER/*psystem_phone_number*/)){
			return OK;
		}
		else {
			LED0_Power(OFF);
			if (Sim900d_SendSMS(PC_NUMBER, "No coordinates!")){
				return ERROR;
			}
		}
		Sim900d_ControlSleep(ON);
	}
	return ERROR;
}


int main(void)
{
	uint32_t cnt=0;

  // ������������� ����������� � ���������
  HardwareInit();

  // ������������� GPS ������

	// ���������, ������������� ������, ����� ����, ����� ��������� ����������

  while (_StartTask()){
		// ���� ������������� �� ���������, ��������� � ���������
		if (_StopTask() == ERROR){
			DelayMs(500);
		}
		// ���� ������������� ����������� �������� 4 ���� ������
		if (cnt == 4){
			// �� �����������
			//Max6_ControlGPS(STOP);
			_ErrorTask();
		}
		cnt++;
	}

	//if (Sim900d_SendSMS("+375298531783", "online") != OK) _ErrorTask();

  // ������� ����
	while (1){
		_MainTask();
	}
  return 0;
}

