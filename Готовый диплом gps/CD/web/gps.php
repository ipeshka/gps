<?php
  mysql_connect('localhost', 'my_login', 'my_password') or die('database connection error!');
  mysql_select_db('gps') or die('database select error!');

    mysql_query("SET character_set_client='utf8'") or die('database query error!');
    mysql_query("SET character_set_results='utf8'") or die('database query error!');
    mysql_query("SET collation_connection='utf8_unicode_ci'") or die('database query error!');

  $phoneID = mysql_real_escape_string( @$_REQUEST['phoneID'] );

  if (!empty( $_REQUEST['latitude'] )) {
    header('Content-Type: text/plain; charset=utf-8');

    $latitude = mysql_real_escape_string( @$_REQUEST['latitude'] );
      $latitude = (float) strtr($latitude, ',', '.');
    $longitude = mysql_real_escape_string( @$_REQUEST['longitude'] );
      $longitude = (float) strtr($longitude, ',', '.');

    mysql_query("INSERT INTO `gps` SET `latitude` = '$latitude', `longitude` = '$longitude',
                 `phoneID` = '$phoneID', `time` = NOW()") or die('database query error!');

    if (is_file(basename(__FILE__, '.php').'.stop')) {
      echo 'stop';
    } else {
      echo 'OK - '.date('[H:i]')." - $latitude/$longitude.\n";
    }

    exit;
  }

  $where = $phoneID ? "`phoneID` = '$phoneID' AND " : '';
  $where .= '`longitude` != 0';
  $max = $_REQUEST['max'] ? $_REQUEST['max'] : 30;
  $q = mysql_query("SELECT * FROM `gps` WHERE $where ORDER BY `time` DESC LIMIT 0, $max");

  $points = array();
  while ($row = mysql_fetch_assoc($q)) {
    $row['time'] = strtotime($row['time']);
    $points[] = $row;
  }

  $lastID = $points[0]['id'];
  $key = 'yamaps-api-key';
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Карта GPS</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="http://api-maps.yandex.ru/1.1/index.xml?key=<?=$key?>" type="text/javascript"></script>

    <script type="text/javascript">
      function Pt(longi, lati) { return new YMaps.GeoPoint(longi, lati); }

      function GoTo(id) {
        window.map.setZoom( 15, {centering: true, position: window.toPt['id' + id]} );
      }

        window.currentID = <?=$lastID?>;

        function GoToAndSetCur(id) { Next(id - window.currentID); }

        function Next(delta) {
          GoTo(window.currentID);
          window.currentID += typeof delta == 'undefined' ? +1 : delta;
          document.getElementById('curID').innerHTML = window.currentID;
        }

        function Prev() { Next(-1); }

      YMaps.jQuery(function () {
        var map = new YMaps.Map(YMaps.jQuery("#map")[0]);
        window.map = map;
        map.setCenter(new YMaps.GeoPoint(30.346044, 59.913387), 12);

        window.toPt = {};
        <? foreach ($points as $point) { ?>
          window.toPt.id<?=$point['id']?> = Pt(<?="$point[longitude], $point[latitude]"?>);
        <? } ?>

        <? foreach ($points as $point) { ?>
          var mark = new YMaps.Placemark(Pt(<?="$point[longitude], $point[latitude]"?>));
              mark.name = "<?=date('H:i (d M Y)', $point['time'])?>";
              mark.description = '<?="#$point[id] | IMEI: $point[phoneID]"?>';
              mark.setIconContent('<?=date("[$point[id]] H:i", $point['time'])?>');
          map.addOverlay(mark);
        <? } ?>
      })
    </script>
  </head>
  <body>
    <div style="margin: 0 auto; text-align: center; width: 1350px">
      <div id="map" style="margin: 0 auto; width: 1300px; height: 1000px"></div>

      <div style="margin-top: 1em">
        Масштаб:
        <button onclick="map.setZoom(map.getZoom() - 1);">-</button>
        <button onclick="map.setZoom(map.getZoom() + 1);">+</button>
        |
        <input type="text" id="markID" size="1" onkeydown="if (event.keyCode == 13) { document.getElementById('markIdBtn').click(); }"/>
        <button id="markIdBtn" onclick="GoToAndSetCur(document.getElementById('markID').value);">Найти по ID</button>
        |
        <button onclick="Prev();">Предыдущая</button>
        &laquo;
        <span onclick="Next(0);" style="border-bottom: 1px dashed; cursor: pointer">
          №<strong><span id="curID"><?=$lastID?></span></strong>
        </span>
        &raquo;
        <button onclick="Next();">Следующая</button>
      </div>

      <table>
        <tr>
          <th colspan="2">
            Точки остановок
          </th>
        </tr>
        <tr>
          <th>ID#</th>
          <th>Время</th>
        </tr>
        <?
          $fuzziness = 0.001;
          $group = true;

            $prev = array(0, 0);
            foreach ($points as $point) {
              $longi = $point['longitude']; $lati = $point['latitude'];
              if (($longi >= $prev[0] - $fuzziness) and ($longi <= $prev[0] + $fuzziness) and
                  ($lati >= $prev[1] - $fuzziness) and ($lati <= $prev[1] + $fuzziness)) {
                if (!$group) {
                  $group = true; ?>
                  <tr> <th colspan="2"><hr /></th> </tr>
                  <tr>
                    <th><?=$prev[2]['id']?></th>
                    <th><?=date('H:i', $prev[2]['time'])?></th>
                  </tr>
                <? } ?>
                <tr>
                  <td><?=$point['id']?></td>
                  <td><?=date('H:i', $point['time'])?></td>
                </tr> <?
              } else {
                $group = false;
              }

              $prev = array($longi, $lati, $point);
            }
        ?>
      </table>
    </div>
  </body>
</html>