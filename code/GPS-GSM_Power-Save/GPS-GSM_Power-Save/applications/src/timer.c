
#include "stm32f10x.h"
#include "timer.h"

/*
 * ������������� �������� ������� � �������� ������������ ��������
 */
void InitBasicTimer (TIM_TypeDef* TIMx){

	if (TIMx == TIM6)	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;   //������ ������������ �� TIM6
	if (TIMx == TIM7) RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;   //������ ������������ �� TIM7

  // ������������ �� 1 ��
  TIMx->PSC = SystemCoreClock / 1000 - 1;
  // ��������� ������������ � ���������� ������ �� ������������
	TIMx->CR1 |= TIM_CR1_ARPE | TIM_CR1_URS;
	// ��������� ���������� �� ������ ��������
	TIMx->CR1 &= (uint16_t)~TIM_CR1_UDIS;
	// ��������� ���������� �� ������������
	TIMx->DIER |= TIM_DIER_UIE;

}


/*
 * ������ �������� �������� ������� � ������� ������������
 */
void PreloadBasicTimer (TIM_TypeDef* TIMx, uint16_t value){
	// �������� �������� � �������� ������������
	TIMx->ARR = value;
	// ��������� �������� �������
	TIMx->EGR = TIM_EGR_UG;
	// �������� ����
	TIMx->SR = ~TIM_SR_UIF;
}


/*
 * ������ �������� �������
 */
void StartBasicTimer (TIM_TypeDef* TIMx){
	TIMx->EGR = TIM_EGR_UG;
	TIMx->CR1 |= TIM_CR1_CEN;
}


/*
 * ��������� �������� �������
 */
void StopBasicTimer (TIM_TypeDef* TIMx){
  // ��������� ������
  TIMx->CR1 &= (uint16_t)(~((uint16_t)TIM_CR1_CEN));
}


/*
 * ����� ��������
 */
void ReloadCounterBasicTimer (TIM_TypeDef* TIMx){
	// ��������� �������� �������
	TIMx->EGR = TIM_EGR_UG;
}
