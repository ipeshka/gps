
void InitBasicTimer (TIM_TypeDef* TIMx);
void PreloadBasicTimer (TIM_TypeDef* TIMx, uint16_t value);
void StartBasicTimer (TIM_TypeDef* TIMx);
void StopBasicTimer (TIM_TypeDef* TIMx);
void ReloadCounterBasicTimer (TIM_TypeDef* TIMx);
