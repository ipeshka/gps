#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "usart.h"
#include "string.h"
#include "max-6.h"
#include "delay.h"
#include "hardware.h"


// ��� ���������� ��������
#define		OK			(0)
#define		ERROR		!(OK)

#define		USART_MAX6		USART1
#define   BUFFER				BufferUsart1

#define		HEADER1				0xB5
#define		HEADER2				0x62

typedef union {
    struct {
        unsigned char     crca;
        unsigned char     crcb;
    };
    unsigned int      crc;

} twobytes;




#define		CFG_INF 			(0x06020A00)
static const uint8_t	CFG_INF_DATA[10] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

#define		CFG_PRT				(0x06001400)
static const uint8_t	CFG_PRT_115200[20] = {/*����� UART*/0x01, 0x00, 0x00, 0x00, /*��������� �����*/0xD0, 0x08, 0x00, 0x00, /*��������*/0x00, 0xC2, 0x01, 0x00, /*�����*/0x03, 0x00, /*�����*/0x03, 0x00, /**/0x00, 0x00, 0x00, 0x00};

#define		CFR_RST				(0x06040400)
static const uint8_t	CFG_RST_STOP[4] = {0xFF, 0x07, 0x08, 0x00};
static const uint8_t	CFG_RST_COLDSTART[4] = {0xFF, 0x87, 0x09, 0x00};
static const uint8_t	CFG_RST_WARMSTART[4] = {0x01, 0x00, 0x09, 0x00};
static const uint8_t	CFG_RST_HOTSTART[4] = {0x00, 0x00, 0x09, 0x00};

#define 	CFG_RATE			(0x06080600)
static const uint8_t	CFG_RATE_3S[6] = {0xB8, 0x0B, 0x01, 0x00, 0x01, 0x00};
static const uint8_t	CFG_RATE_1S[6] = {0xE8, 0x03, 0x01, 0x00, 0x01, 0x00};


/*
 * �������������
 */
void Max6_Init (void){
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Pin = MAX6_EXTINT0;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIO_MAX6_EXTINT0, &GPIO_InitStructure);
  GPIO_WriteBit (GPIO_MAX6_EXTINT0, MAX6_EXTINT0, Bit_RESET);

  GPIO_WriteBit (GPIO_MAX6_EXTINT0, MAX6_EXTINT0, Bit_SET);
	DelayMs(500);
	// ��������� ������ �� 115200
	Max6_WriteCmd(CFG_PRT, CFG_PRT_115200);
	DelayMs(100);

  // ������������� ����
	USART_Cmd(USART_MAX6, DISABLE);
  USART_StructInit(&USART_InitStructure);
  USART_Init(USART_MAX6, &USART_InitStructure);
	USART_Cmd(USART_MAX6, ENABLE);
  DelayMs(500);
}


/*
 * �������� � ������
 */
void PutsToMax6 (uint8_t * data, uint32_t length){
	static volatile struct TBufferUsart *buffer = &BUFFER;
	volatile uint8_t *txbuffer = (uint8_t *)(buffer->txbuf);
	uint32_t i;

	i = length;
	while (i--){
		*txbuffer++ = *data++;
	}
	buffer->txlength = length;

	SendBuf(USART_MAX6);
}


/*
 * �������� ������� � ������
 * cmd - ��� �������: 1 ���� Class, 2-�� ID, 3,4 - Length
 * data - ������
 */
void Max6_WriteCmd (uint32_t cmd, const uint8_t *data){
	uint8_t buffer[32];
	uint8_t *pbuffer = buffer;
	// ��������� �� ������ ������� �������,
	// �.�. ��������� � ������� CRC �� ���������
	uint8_t *pbuffer_crc = &buffer[2];
	uint32_t i;
	uint32_t cnt = 0;
	uint32_t length;
	uint16_t crc;

	*pbuffer++ = HEADER1;
	*pbuffer++ = HEADER2;

	for (i = 4; i; i--){
		*pbuffer++ = cmd >> (8 * (i - 1)) & 0xFF;
		if (i == 2)	length = *(pbuffer - 1);
		cnt++;
	}

	for (i = length; i; i--){
		*pbuffer++ = *data++;
		cnt++;
	}

	crc = CalcCRC(pbuffer_crc, cnt);
	*pbuffer++ = crc & 0xFF;
	*pbuffer = crc >> 8;
	PutsToMax6(buffer, cnt + 4);
}


/*
 * ���� � ���������
 */
void GetsMax6 (void){
	Gets (USART_MAX6);
}


/*
 * ����� uint8_t ����� � ��������
 */
void PutsDec8ToMax6(uint8_t decnum){
	PutsDec8(USART_MAX6, decnum);
}


/*
 * ����� uint32_t ����� � ��������
 */
void PutsDec32ToMax6(uint32_t decnum){
	PutsDec32(USART_MAX6, decnum);
}


/*
 * ������� ��������� ���� �������� ������ �� PC � ����������
 */
int8_t Max6_CompareStr (void *str, uint32_t *pos){
	uint32_t p;
	int8_t result;
	volatile struct TBufferUsart *buffer = &BUFFER;

	GetsMax6();
	result = CompareStrBuffer((void *)buffer->rxbuf, str, &p);
	*pos = p;
	return result;
}


/*
 *
 */
int8_t Max6_CompareAll (void *str){
	static volatile struct TBufferUsart *buffer = &BUFFER;
	return CompareAll ((void *)buffer->rxbuf, str);
}


/*
 * ������� ���������� ���������� �� ����� � PC
 */
void USART_Max6_Off (void){
	//IT_RX_Disable(USART_MAX6);
	USARTx_Disable(USART_MAX6);
}


/*
 * ������� ��������� ���������� �� ����� � PC
 */
void USART_Max6_On (void){
	//IT_RX_Enable(USART_MAX6);
	USARTx_Enable(USART_MAX6);
}


/*
 * ������ CRC
 */
uint16_t CalcCRC (uint8_t *data, uint32_t length){
  twobytes cksum;
  cksum.crc = 0x0000;
	uint32_t i;
	uint8_t *pdata = data;

	for(i = 0; i < length; i++)
	{
		cksum.crca += pdata[i];
		cksum.crcb += cksum.crca;
	}
	return cksum.crc;
}


/*
 * ���������� GPS �������
 */
void Max6_ControlGPS (TControl control){
	static const uint8_t *cmd;

	switch (control){
	case STOP:
		cmd = &CFG_RST_STOP[0];
		break;
	case COLDSTART:
		cmd = &CFG_RST_COLDSTART[0];
		break;
	case WARMSTART:
		cmd = &CFG_RST_WARMSTART[0];
		break;
	case HOTSTART:
		cmd = &CFG_RST_HOTSTART[0];
		break;
	}
	Max6_WriteCmd(CFR_RST, cmd);
	GetsMax6();
	// ����������� ������ ������ ����� �������� ���������� � �.
  Max6_WriteCmd(CFG_RATE, CFG_RATE_1S);
  GetsMax6();
}


/*
 * ������ ��������� � ������ GPS
 */
int8_t Max6_ReadCoordinate (int8_t *msgx, int8_t *msgy, int8_t *msgtime){
	static volatile struct TBufferUsart *buffer = &BUFFER;
	uint32_t pos;
	uint32_t cnt = 0;
	uint8_t i = 0;

	while (Max6_CompareStr("$GPGLL,,", &pos) == OK){
		// ���� � ������� 45 �. ��������� �� �������, �������
		if (cnt == 45)	return ERROR;
		cnt++;
	}
	USART_Max6_Off();

	// ��������� ������
	while (buffer->rxbuf[pos] != ','){

		/* �� ������ � ������ ������� �������� i, point, tmp1, tmp2, ���� �� ����������� � �� ������� :)
		*/
		/*
				for (i = 0; msg[i] != '.'; i++ );
				tmp1 = msg[i-2];
				tmp2 = msg[i-1] ;
				point = msg[i];
				msg[i-2] = point;
				msg[i-1] = tmp1;
				msg[i]= tmp2;
		*/
		// ���������� ����� �� 2 ����� �����
		if (buffer->rxbuf[pos] == '.'){
			*(msgx - 2) = '.';
			*(msgx - 1) = buffer->rxbuf[pos - 2];
			*msgx++ = buffer->rxbuf[pos - 1];
			pos++;
		}
		else	*msgx++ = buffer->rxbuf[pos++];
	}
	pos+=1;
	*msgx++ = buffer->rxbuf[pos];
	pos+=2;
	// ��������� �������
	while (buffer->rxbuf[pos] != ','){
		// ���������� ����� �� 2 ����� �����
		if (buffer->rxbuf[pos] == '.'){
			*(msgy - 2) = '.';
			*(msgy - 1) = buffer->rxbuf[pos - 2];
			*msgy++ = buffer->rxbuf[pos - 1];
			pos++;
		}
		else	*msgy++ = buffer->rxbuf[pos++];
	}
	pos+=1;
	*msgy++ = buffer->rxbuf[pos];
	pos+=2;
	// ��������� �����
	while (buffer->rxbuf[pos] != '.'){
		*msgtime++ = buffer->rxbuf[pos++];
		if ((i == 1) || (i == 3)) *msgtime++ = ':';
		i++;
	}
	*msgtime++ = ' ';
	*msgtime++ = 'U';
	*msgtime++ = 'T';
	*msgtime = 'C';
	USART_Max6_On();

	return OK;
}
