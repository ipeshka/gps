
/*
#define   SYSTEM_NUMBER		"+79041397842"
#define   PIN         		"9064"
*/

#define   SYSTEM_NUMBER		"+79041397846"
#define   PIN         		"9523"


#define   PC_NUMBER				"+79526331138"/*"+79041397842"*/ /*"+79501200136"*/

#define   CMD_READ				"read"


#define GPIO_SIM900D_PWRKEY     GPIOB
#define GPIO_SIM900D_STATUS     GPIOB
#define GPIO_SIM900D_DTR     		GPIOA
#define SIM900D_STATUS          GPIO_Pin_1
#define SIM900D_PWRKEY          GPIO_Pin_0
#define SIM900D_DTR          		GPIO_Pin_4


#define SIM900D_SIZE_BUFFER     556


int8_t Sim900d_Power (TPower power);
void GPIO_Sim900d_Init (void);
int8_t Sim900d_Init (void);
void PutsToSim900 (const void *str);
void PutCharToSim900 (uint8_t data);
void GetsSim900 (void);
void PutsDec8ToSim900(uint8_t decnum);
int8_t Sim900d_WaitingResponse (void);
int8_t Sim900d_CompareStr (void *str, uint32_t *pos);
int8_t Sim900d_CompareAll (void *str);
uint32_t Sim900d_FindStr (void *str);
int8_t Sim900d_WriteCmdWithCheck (const void *cmd);
void Sim900d_WriteCmd (const void *cmd);
int8_t Sim900d_WritePIN (char *pin);
uint8_t Sim900d_ReadLevelSignal (void);
int8_t Sim900d_ReadNameOpsos (int8_t* name);
int8_t Sim900d_SendSMS (void * number, void* msg);
int8_t Sim900d_ReadSMS(uint8_t index, int8_t* number, int8_t* date, int8_t* time, int8_t* msg);
uint8_t Sim900d_EventNewSMS(void);
int8_t Sim900d_DelIndexSMS(uint8_t index);
void USART_Sim900d_Off (void);
void USART_Sim900d_On (void);
void Sim900d_ControlSleep (TPower control);
