#include "stm32f10x.h"
#include "hardware.h"
#include "usart.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_usart.h"
#include "sim900d.h"
//#include "misc.h"




/*
 * ��������� ����������� ����������
 */
static void _NVIC_Configuration(void)
{
  //NVIC_InitTypeDef NVIC_InitStructure;

  /* Configure the NVIC Preemption Priority Bits */
  //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_SetPriorityGrouping(0);

  /* Enable the USART1 Interrupt */
  /*NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);*/



  /* Enable the USART2 Interrupt */
  /*NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);*/

  // ���������� ����������
  NVIC_SetPriority(USART1_IRQn, 0);
  NVIC_SetPriority(USART2_IRQn, 2);
  NVIC_SetPriority(TIM6_DAC_IRQn, 1);
  NVIC_SetPriority(TIM7_IRQn, 3);

  // ��������� ����������
  NVIC_EnableIRQ(USART1_IRQn);
  NVIC_EnableIRQ(USART2_IRQn);
  NVIC_EnableIRQ(TIM6_DAC_IRQn);
  NVIC_EnableIRQ(TIM7_IRQn);

}


/*
 * ������������� ����� LED
 */
static void _LED0_Init (void){
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Pin = LED0;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIO_LED0, &GPIO_InitStructure);
  GPIO_WriteBit (GPIO_LED0, LED0, Bit_RESET);
}


/*
 * ���������/���������� LED
 */
void LED0_Power (TPower power){
	GPIO_WriteBit (GPIO_LED0, LED0, power);
}


/*
 * ��������� ��
 */
void HardwareInit(void)
{
  // ��������� ��������� ������
  if (SysTick_Config(SystemCoreClock / 1000))
  {
    /* Capture error */
    while (1);
  }
  // �������� ���� APB2
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  // ��������������� ����� PD0 � PD1 ��� ������
  GPIO_PinRemapConfig(GPIO_Remap_PD01, ENABLE);
  // ������������� LED �����
  _LED0_Init();
  // ������������ USART1
  USART1_Init();
  // ������������ USART2
  USART2_Init();
  GPIO_Sim900d_Init();
  // ������������� ����������� ����������
  _NVIC_Configuration();
}





