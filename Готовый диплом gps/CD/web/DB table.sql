CREATE TABLE `gps` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `phoneID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` datetime NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `time+phoneID` (`time`,`phoneID`),
  KEY `phoneID` (`phoneID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


