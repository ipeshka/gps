/**
  ******************************************************************************
  * @file    USART/Interrupt/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and peripherals
  *          interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"
#include "stm32f10x_usart.h"
#include "delay.h"
#include "usart.h"
#include "timer.h"


#include "hardware.h"
//#include "stm32f10x_gpio.h"



/** @addtogroup STM32F10x_StdPeriph_Examples
  * @{
  */

/** @addtogroup USART_Interrupt
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
/*void HardFault_Handler(void)
{
  //Go to infinite loop when Hard Fault exception occurs
  while (1)
  {
  }
}*/

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSV_Handler exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  TimingDelay_Decrement();
}

/******************************************************************************/
/*            STM32F10x Peripherals Interrupt Handlers                        */
/******************************************************************************/

/**
  * @brief  This function handles USART1 global interrupt request.
  * @param  None
  * @retval None
  */
void USART1_IRQHandler(void)
{

  volatile struct TBufferUsart *buffer;
  buffer = &BufferUsart1;

  if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
  {
  	//LED0_Power(1);
  	if (buffer->rxflag == RX_NO_COMPLETE)		{
  		buffer->rxflag = RX_BUSY;
  	}
    if (buffer->rxlength == RX_BUF_SIZE) {
    	buffer->rxflag = RX_ERROR;
      USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
    }
    // Read one byte from the receive data register
    buffer->rxbuf[buffer->rxlength] = USART_ReceiveData(USART1);

    buffer->rxlength++;

    USART_Timeout(USART1);

  }
  
	if(USART_GetITStatus(USART1, USART_IT_TXE) != RESET) {

	  if (buffer->txcnt == TX_BUF_SIZE){
	  	buffer->txflag = TX_ERROR;
	  	USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
	  }
	  if (buffer->txcnt != buffer->txlength) {
			// Write one byte to the transmit data register
			USART_SendData(USART1, buffer->txbuf[buffer->txcnt]);
		  buffer->txcnt++;
	  }
	  else
		{
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
	  	buffer->txflag = TX_COMPLETE;
	  	buffer->txcnt = 0;
	  	buffer->txlength = 0;
		}

	}

	if(USART_GetITStatus(USART1, USART_IT_TC) != RESET){
		USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		USART_ClearITPendingBit(USART1, USART_IT_TC);
	}

  if (USART_GetITStatus(USART1, USART_FLAG_CTS) !=  RESET)
  {
    USART_ClearITPendingBit(USART1, USART_IT_CTS);
  }
}

/**
  * @brief  This function handles USART2 global interrupt request.
  * @param  None
  * @retval None
  */
void USART2_IRQHandler(void)
{
  volatile struct TBufferUsart *buffer;
  buffer = &BufferUsart2;
  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
  {
  	//LED0_Power(1);
  	if (buffer->rxflag == RX_NO_COMPLETE)		{
  		buffer->rxflag = RX_BUSY;
  	}
    if (buffer->rxlength == RX_BUF_SIZE) {
    	buffer->rxflag = RX_ERROR;
      USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
    }
    // Read one byte from the receive data register
    buffer->rxbuf[buffer->rxlength] = USART_ReceiveData(USART2);

    buffer->rxlength++;

    USART_Timeout(USART2);


  }

	if(USART_GetITStatus(USART2, USART_IT_TXE) != RESET) {

	  if (buffer->txcnt == TX_BUF_SIZE){
	  	buffer->txflag = TX_ERROR;
	  	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
	  }
	  if (buffer->txcnt != buffer->txlength) {
			// Write one byte to the transmit data register
			USART_SendData(USART2, buffer->txbuf[buffer->txcnt]);
		  buffer->txcnt++;
	  }
	  else
		{
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
	  	buffer->txflag = TX_COMPLETE;
	  	buffer->txcnt = 0;
	  	buffer->txlength = 0;
		}
	}

	if(USART_GetITStatus(USART2, USART_IT_TC) != RESET){
		USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		USART_ClearITPendingBit(USART2, USART_IT_TC);
	}

  if (USART_GetITStatus(USART2, USART_FLAG_CTS) !=  RESET)
  {
    USART_ClearITPendingBit(USART2, USART_IT_CTS);
  }
}

/**
  * @brief  This function handles TIM6/DAC global interrupt request.
  * @param  None
  * @retval None
  */
void TIM6_DAC_IRQHandler()
{
  volatile struct TBufferUsart *buffer;
  buffer = &BufferUsart1;
  if(TIM6->SR & TIM_SR_UIF)
  {
    // ���������� ��� ��������������� ����������
		TIM6->SR = ~TIM_SR_UIF;
    // ��������� ������
		StopBasicTimer(TIM6);
    // ��������� ����� ������ �� USART1
    buffer->rxflag = RX_COMPLETE;
    buffer->rxbuf[buffer->rxlength] = '\0';
    buffer->rxlength = 0;

    //LED0_Power(0);
  }
}

/**
  * @brief  This function handles TIM6/DAC global interrupt request.
  * @param  None
  * @retval None
  */
void TIM7_IRQHandler()
{
  volatile struct TBufferUsart *buffer;
  buffer = &BufferUsart2;

	// ���������� ��� ��������������� ����������
	TIM7->SR = ~TIM_SR_UIF;
	// ��������� ������
	StopBasicTimer(TIM7);
	// ��������� ����� ������ �� USART1
	buffer->rxflag = RX_COMPLETE;
	buffer->rxbuf[buffer->rxlength] = '\0';
	buffer->rxlength = 0;
	//LED0_Power(0);
}

/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
