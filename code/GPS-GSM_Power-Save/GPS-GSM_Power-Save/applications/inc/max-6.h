

#define 	GPIO_MAX6_EXTINT0     		GPIOA
#define		MAX6_EXTINT0							GPIO_Pin_8

typedef enum {STOP = 0, COLDSTART, WARMSTART, HOTSTART} TControl;

void Max6_Init (void);
void PutsToMax6 (uint8_t * data, uint32_t length);
void PutsDec8ToMax6(uint8_t decnum);
void PutsDec32ToMax6(uint32_t decnum);
void Max6_WriteCmd (uint32_t cmd, const uint8_t *data);
void GetsMax6 (void);
int8_t Max6_CompareAll (void *str);
int8_t Max6_CompareStr (void *str, uint32_t *pos);
void USART_Max6_Off (void);
void USART_Max6_On (void);
uint16_t CalcCRC (uint8_t *data, uint32_t length);
void Max6_ControlGPS (TControl control);
int8_t Max6_ReadCoordinate (int8_t *msgx, int8_t *msgy, int8_t *msgtime);
