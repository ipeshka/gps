#include "stm32f10x.h"


#define		OK			(0)
#define		ERROR		!(OK)


/*
 * ����������� ���������� ����� � ����� long
 */
uint32_t ConvertStringToNumber (void * str){
	uint8_t *s = (uint8_t *) str;
	uint32_t number = 0;
	uint32_t multiplier = 1;
	uint8_t value = 0;

	while (*s){
		s++;
	}
	--s;
	while (s != (str - 1)){
		switch (*s--){
		case '0':
			value = 0;
			break;
		case '1':
			value = 1;
			break;
		case '2':
			value = 2;
			break;
		case '3':
			value = 3;
			break;
		case '4':
			value = 4;
			break;
		case '5':
			value = 5;
			break;
		case '6':
			value = 6;
			break;
		case '7':
			value = 7;
			break;
		case '8':
			value = 8;
			break;
		case '9':
			value = 9;
			break;

		default:
			break;
		}
		// ��������������� �����
		number = number + value * multiplier;
		multiplier *= 10;
	}
	return number;
}


/*
 * ������� ��������� �������� ������ � ����������
 * ���������� OK � ������ ������� ����������
 * ����� ERROR
 */
int8_t CompareAll (void *buf, void *str){
	int8_t *s = (int8_t *) str;
	int8_t *b = (int8_t *) buf;

	// ���������� ��� ������
	while (*s)
	{
		// ���� ������� ������������ -
		// ���������� ��� ������
		if (*b++ != *s++) {
			return ERROR;
		}
	}
	return OK;
}


/*
 * ������� ������ ���������� �������� ������ � ����������
 * ���������� OK � ������, ���� ������ ������� �������
 * ����� ERROR
 */
int8_t CompareStrBuffer (void *buf, void *str, uint32_t *pos){
	int8_t *s = (int8_t *) str;
	int8_t *b = (int8_t *) buf;
	uint32_t i = 0;
	uint8_t flag = 0;
	// ���������� ���� ����� �� ������� '\0'
	while (b[i]){

		// ���� ���������� � ������ �������� � ������
		if (b[i] == *s){
			// � ��������� ������� ������� ������� � ������
			*pos = i;
			// ���������� ��� ������
			while (*++s){
				++(*pos);
				// ���� ������� ��������������, �� ���������� ����, �������������� ���������
				// � ��������� �� ��������� �������� �����
				// � �.�. �� ���������� ����� ������
				if (b[*pos] != *s){
					flag = 1;
					s = (int8_t *) str;
					break;
				}
			}

			if (!flag) {
				--(*pos);
				return OK;
			}
			else flag = 0;
		}
		i++;
	}
	return ERROR;
}

/*int8_t CompareStrBuffer (void *buf, void *str, uint32_t *pos){
	int8_t *s = (int8_t *) str;
	int8_t *b = (int8_t *) buf;
	uint32_t i = 0;
	uint8_t flag = 0;
	// ���������� ���� ����� �� ������� '\0'
	while (*b++){

		// ���� ���������� � ������ �������� � ������
		if (*b == *s){
			// � ��������� ������� ������� ������� � ������
			*pos = i;
			++s;
			// ���������� ��� ������
			while (*s){
				// ���� ������� ��������������, �� ���������� ����, �������������� ���������
				// � ��������� �� ��������� �������� �����
				// � �.�. �� ���������� ����� ������
				if (b[(*pos)++] != *s++){
					flag = 1;
					s = (int8_t *) str;
					break;
				}
			}

			if (!flag) {
				++(*pos);
				return OK;
			}
			else flag = 0;
		}
		i++;
	}
	return ERROR;
}*/


/*
 * ����� �������
 * ���������� ������� � ������� ���������� ���������� �������
 */
uint32_t FindStr (void *buf, void *str){
	char *s = (char *)str;
	char *b = (char *)buf;
	uint32_t pos = 0;
	uint32_t i = 0;
	uint8_t flag = 0;

	// ���������� ���� ����� �� ������� '\0'
	while (b[i]){
	// ���� ���������� � ������ �������� � ������
		if (b[i] == *s){
			// � ��������� ������� ������� ������� � ������
			pos = i;
			// ���������� ��� ������
			while (*++s){
				++pos;
				// ���� ������� ��������������, �� ���������� ����, �������������� ���������
				// � ��������� �� ��������� �������� �����
				// � �.�. �� ���������� ����� ������
				// ���������� ��������� �������
				if (b[pos] != *s){
					flag = 1;
					s = str;
					break;
				}
			}
			if (!flag) return pos;
			else flag = 0;
		}
		i++;
	}
	return 0;
}
