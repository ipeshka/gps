#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "hardware.h"
#include "sim900d.h"
#include "delay.h"
#include "usart.h"
#include "max-6.h"
#include "string.h"
#include <stdio.h>



// ��� ���������� ��������
/*
#define		OK			(0)
#define		ERROR		!(OK)
*/

#define		TIMEOUT_REG_NETWORK		90	// ����� �������� ����������� � ���� � �.


#define		USART_GSM		USART2
#define   BUFFER			BufferUsart2


// ������ ����� ������
#define		ReadNameModule		("AT+GOI\r")
// ���������� ���
#define		EchoOff						("ATE0\r")
// ������� ������ ���� ������
#define		ReturnResponse		("ATV0=0\r")
// ������ ����� ������
#define		ReadNameOpsos			("AT+COPS?\r")
// ������ ������ �������
#define   ReadLevelSignal		("AT+CSQ\r")
// ������ ��������� ����������� � ����
#define   ReadNetworkRegistration		("AT+CREG?\r")
// ������, ��������� ���� PIN ���� ��� ���
#define   ReadStatusPin			("AT+CPIN?\r")
// ������ PIN ����
#define		WritePin 					("AT+CPIN=")
// �������� ��������� �����
#define   TextMode					("AT+CMGF=1\r")
// GSM ���������
#define 	CharacterSetGSM		("AT+CSCS=\"GSM\"\r")
// ��������� ����� ��� ������
#define 	DisableErrorCode	("AT+CMEE=0\r")
// ������ SMS �� ������
#define 	ReadSMSIndex			("AT+CMGR=")
// �������� ���� SMS
#define   DelAllSMS					("AT+CMGD=1,4\r")
// �������� SMS �� �������
#define   DelIndexSMS				("AT+CMGD=")
// ������ ���� �������� �������
#define   DisableAllCall		("AT+GSMBUSY=1\r")
// ������ URC ���������
#define   DisableURC				("AT+CIURC=0\r")
// �������� ������ ����������������
#define   FullFunctionality	("AT+CFUN=1\r")
// �������� ����� Sleep Mode 1
#define   EnableSleepMode1	("AT+CSCLK=1\r")
// ��������� ����� Sleep
#define   DisableSleepMode		("AT+CSCLK=0\r")


/*
 * ���������/���������� ������ � ��������� ��� �����������������
 */
int8_t Sim900d_Power (TPower power)
{
	uint8_t status;
	uint32_t cnt = 0;
	status = GPIO_ReadInputDataBit(GPIO_SIM900D_STATUS, SIM900D_STATUS);
	if (status != power){
		USART_Sim900d_Off();
		// �������� ����� �����������, ����� ������ �������� ���� ����.
		// �����, ���� ������� ��� ������ ���������� ����� ����������� ������ ��� ���������,
		// �� �� �� ����������. ������ ����� ������, � ���� ���������� � �����.
		// ����� *���.
		DelayMs(500);
	  GPIO_WriteBit (GPIO_SIM900D_PWRKEY, SIM900D_PWRKEY, Bit_RESET);
	  DelayMs(2000);
	  GPIO_WriteBit (GPIO_SIM900D_PWRKEY, SIM900D_PWRKEY, Bit_SET);
	  // ������� ���������/���������� SIM900
		while (GPIO_ReadInputDataBit(GPIO_SIM900D_STATUS, SIM900D_STATUS) == status){
			DelayMs(1);
			// ���� �� ���������� 3 ������ ������ �� ���������, ������ ������ �����
			if (++cnt == 3000)	return ERROR;
		}
		DelayMs(200);
		USART_Sim900d_On();
	}
	return OK;
}


/*
 * ������������� ������ ��, ������������ ��� ������ � �������
 */
void GPIO_Sim900d_Init (void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable the GPIO Clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
  GPIO_InitStructure.GPIO_Pin = SIM900D_PWRKEY;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIO_SIM900D_PWRKEY, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_InitStructure.GPIO_Pin = SIM900D_STATUS;
  GPIO_Init(GPIO_SIM900D_STATUS, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
  GPIO_InitStructure.GPIO_Pin = SIM900D_DTR;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIO_SIM900D_DTR, &GPIO_InitStructure);

  GPIO_WriteBit (GPIO_SIM900D_DTR, SIM900D_DTR, Bit_RESET);
  GPIO_WriteBit (GPIO_SIM900D_PWRKEY, SIM900D_PWRKEY, Bit_SET);
}

/*
 * ������������� ������
 */
int8_t Sim900d_Init (void){
	uint32_t pos;
	// ��������� ���
	if (Sim900d_WriteCmdWithCheck(EchoOff))	return ERROR;
	// ������ ����������������
	if (Sim900d_WriteCmdWithCheck(FullFunctionality))	return ERROR;
	// �������� Sleep
	if (Sim900d_WriteCmdWithCheck(EnableSleepMode1))	return ERROR;
	// �������� ������� � Sleep
	//Sim900d_ControlSleep(OFF);
	// ������ PIN, ���� ����������
	if (Sim900d_WritePIN(PIN))	return ERROR;

	// ������� ����������� � ����
	do{
		// ������� . ������ �������
		if (Wait(TIMEOUT_REG_NETWORK) == 1){
			return ERROR;
		}
		// ������ ��������� ����������� � ����
		Sim900d_WriteCmd(ReadNetworkRegistration);
		// ���������� ��������� ����������� � ���� �� ��� ���,
		// ���� ������ �� ������ "+CREG: 0,2", �.�.
		// ��������������� � �������� ����
	} while (Sim900d_CompareStr("+CREG: 0,1", &pos) != OK);
	// ������ ���� �������� �������
	if (Sim900d_WriteCmdWithCheck(DisableAllCall))	return ERROR;
	// ������� ��� SMS
	if (Sim900d_WriteCmdWithCheck(DelAllSMS))	return ERROR;
	return OK;
}


/*
 * ���� � GSM ������
 */
void PutsToSim900 (const void *str){
	Puts (USART_GSM, str);
}


/*
 * ����� � GSM ������
 */
void GetsSim900 (void){
	Gets (USART_GSM);
}


/*
 * ����� ����� � GSM ������
 */
void PutCharToSim900 (uint8_t data){
	Put_Char(USART_GSM, data);
}


/*
 * ����� ����� � ��������
 */
void PutsDec8ToSim900 (uint8_t decnum){
	PutsDec8(USART_GSM, decnum);
}


/*
 * ������� � ������� ��������� ������� ��������� � ������������ ������� ������������ �������
 * ���������� OK/ERROR
 */
int8_t Sim900d_WaitingResponse (void){
	uint32_t pos;

	return Sim900d_CompareStr("\r\nOK", &pos);
}


/*
 * ������� ������ ���������� ������ �� ������ � ����������
 */
int8_t Sim900d_CompareStr (void *str, uint32_t *pos){
	uint32_t p;
	int8_t result;
	volatile struct TBufferUsart *buffer = &BUFFER;

	GetsSim900();
	result = CompareStrBuffer((void *)buffer->rxbuf, str, &p);
	*pos = p;
	return result;

}


/*
 * ������� ��������� ���� �������� ������ �� ������  � ����������
 */
int8_t Sim900d_CompareAll (void *str){
	volatile struct TBufferUsart *buffer = &BUFFER;
	GetsSim900();
	return CompareAll ((void *)buffer->rxbuf, str);
}


/*
 * ����� ������� � �������� ������ ������
 * ���������� ������� � ������� ���������� ���������� �������
 */
uint32_t Sim900d_FindStr (void *str){
	volatile struct TBufferUsart *buffer = &BUFFER;
	return FindStr ((char *)buffer->rxbuf, str);
}


/*
 * ���������� ������� ������ � ������
 * ���������� ��� � ���������� ��������: OK/ERROR
 */
int8_t Sim900d_WriteCmdWithCheck (const void *cmd){
	Sim900d_WriteCmd(cmd);
	return Sim900d_WaitingResponse();
}


/*
 * ���������� ������� ������ � ������
 */
void Sim900d_WriteCmd (const void *cmd){
	PutsToSim900(cmd);
}


/*
 * ����������, ��������� �� PIN ���
 * � ������ ���
 * ���������� ��������� �������� OK/ERROR
 */
int8_t Sim900d_WritePIN (char *pin){
	DelayMs(500);
	// ������ ������ PIN ����
	if (Sim900d_WriteCmdWithCheck(ReadStatusPin))	return ERROR;
	// ���� ��������� PIN, �������� ��� � ��������� ������� �� OK
	if (Sim900d_FindStr("SIM PIN\r") > 0){
		Sim900d_WriteCmd(WritePin);
		Sim900d_WriteCmd(pin);
		if (Sim900d_WriteCmdWithCheck("\r"))	return ERROR;
	}
	return OK;
}


/*
 * ���������� � % ������� �������
 */
uint8_t Sim900d_ReadLevelSignal (void){
	volatile struct TBufferUsart *buffer = &BUFFER;
	const int8_t size = 3;
	int8_t signal[size];
	uint8_t value = 0;
	uint8_t i = 0;
	uint32_t pos;

	Sim900d_WriteCmd (ReadLevelSignal);
	if (!Sim900d_CompareStr("+CSQ: ", &pos) ){
		++pos;
		for (i = 0; i < size - 1; i++) {
			signal[i] = buffer->rxbuf[pos++];
		}
		value = ConvertStringToNumber(signal);
		if ((value < 2) && (value == 99))	return 0;
		else value = value * 100 / 31;
	}
	return value;
}



/*
 * ���������� SMS
 */
int8_t Sim900d_SendSMS (void * number, void* msg){
	uint32_t pos;
	//LED0_Power(ON);
	// ������� ������ � ����� ������ � ��������� �����������
	if (Sim900d_WriteCmdWithCheck(TextMode))	return ERROR;
	// �������� ��������� GSM
	//LED0_Power(OFF);
	if (Sim900d_WriteCmdWithCheck(CharacterSetGSM))	return ERROR;
	Sim900d_WriteCmd("AT+CMGS=\"");
	Sim900d_WriteCmd(number);
	Sim900d_WriteCmd("\"\r");

	// ������� ������ ���������� ����� ������
	if (Sim900d_CompareStr(">", &pos))	return ERROR;
	Sim900d_WriteCmd(msg);

	// ���������� ���������� ���������  ����� Ctrl+Z
	PutCharToSim900(CTRL_Z);
	// ���� ��� �������������, ���������� ��� ������
	if (Sim900d_CompareStr("OK", &pos))	return ERROR;

	return OK;
}


/*
 * ������� SMS � ��������� ��������
 */
int8_t Sim900d_DelIndexSMS(uint8_t index){
	Sim900d_WriteCmd(DelIndexSMS);
	PutsDec8ToSim900(index);
	return Sim900d_WriteCmdWithCheck("\r");
}

/*
 * ��������� ������ ������ ���������
 */
uint8_t Sim900d_EventNewSMS(void){
	volatile struct TBufferUsart *buffer = &BUFFER;
	int8_t ch[4] = {'\0', '\0', '\0', '\0'};
	uint8_t i = 0;
	uint32_t pos;
	if (buffer->rxflag == RX_COMPLETE){
		if (!Sim900d_CompareStr("+CMTI: \"SM\",", &pos)){
			pos = pos + 2;
			while (buffer->rxbuf[pos] != '\r'){
				ch[i++] = buffer->rxbuf[pos++];
			}
			return ConvertStringToNumber(ch);
		}
	}
	return 0;
}


/*
 * ������� ����� ������ ���������
 */
/*void Puts_Sim900d_NewSMS (void){

}*/


/*
 * ������ SMS �� �������
 */
int8_t Sim900d_ReadSMS(uint8_t index, int8_t* number, int8_t* date, int8_t* time, int8_t* msg){
	volatile struct TBufferUsart *buffer = &BUFFER;
	uint32_t pos, end_pos;

	Sim900d_WriteCmd(ReadSMSIndex);
	PutsDec8ToSim900(index);
	Sim900d_WriteCmd(",0\r");
	// ���� ������� ������� � �������� SMS
	if (!Sim900d_CompareStr("+CMGR", &pos)){
		// ���� ������, � �������� ���������� ����� ��������
		pos = Sim900d_FindStr("\",\"+");
		end_pos = pos + 12;
		while (pos < end_pos){
			*number++ = buffer->rxbuf[pos++];
		}
		*number = '\0';
		// ���� ������, � �������� ���������� ����
		pos = pos + 6;

		// ���� �������, �� ������� ������������� ����
		end_pos = pos + 8;
		while (pos < end_pos){
			*date++ = buffer->rxbuf[pos++];
		}
		*date = '\0';

		// ���� �������, �� ������� ������������� �����
		pos = pos + 1;
		end_pos = pos + 8;
		while (pos < end_pos){
			*time++ = buffer->rxbuf[pos++];
		}
		*time = '\0';
		// ���� �������, � �������� ���������� ����� SMS
		pos = Sim900d_FindStr("\"\r\n") + 1;
		while (buffer->rxbuf[pos]){
			*msg++ = buffer->rxbuf[pos++];
		}
		*msg = '\0';
	}

	return OK;
}


/*
 * ������ ��� ���������
 */
int8_t Sim900d_ReadNameOpsos (int8_t* name){
	volatile struct TBufferUsart *buffer = &BUFFER;
	uint32_t pos;
	uint32_t end_pos;

	// ���������� ������� �� ������ �����
	// ��������� �� ������� OK � ������
	if (Sim900d_WriteCmdWithCheck (ReadNameOpsos))	return ERROR;
	// ���� ������ ������, � �������� ���������� ��� ������
	pos = Sim900d_FindStr(",\"");
	// ���� �������, �� ������� ������������� ��� ������
	end_pos = Sim900d_FindStr("\"\r");
	while (pos < end_pos){
		*name++ = buffer->rxbuf[pos++];
	}
	*name = '\0';
	return OK;
}


/*
 * ��������� USART
 */
void USART_Sim900d_Off (void){
	USARTx_Disable(USART_GSM);
}


/*
 * �������� USART
 */
void USART_Sim900d_On (void){
	USARTx_Enable(USART_GSM);
}


/*
 * ��������/��������� Sleep ������
 */
void Sim900d_ControlSleep (TPower control){
	switch (control){
	case OFF:
		GPIO_WriteBit (GPIO_SIM900D_DTR, SIM900D_DTR, Bit_RESET);
		DelayMs(70);
		break;
	case ON:
		GPIO_WriteBit (GPIO_SIM900D_DTR, SIM900D_DTR, Bit_SET);
		break;
	default:
		break;
	}
}
